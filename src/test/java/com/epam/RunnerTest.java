package com.epam;

import org.junit.Assert;
import org.junit.Test;

public class RunnerTest {

    private Runner runner = new Runner();

    @Test
    public void test() {
        Assert.assertEquals(runner.getMessage(), "Heldlo, world!");
    }
}
